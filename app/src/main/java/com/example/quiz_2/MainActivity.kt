package com.example.quiz_2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.example.quiz_2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var deleteUserLen: Int = 0
    private var userList: MutableMap<String, List<String>> = mutableMapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.addUser.setOnClickListener(addUserClick)
        binding.removeUser.setOnClickListener(removeUserClick)
        binding.updateUser.setOnClickListener(updateUserClick)
    }

    private fun isAllFieldsValid(): Boolean {
        return !(binding.emailAddress.text.isEmpty() ||
                binding.firstName.text.isEmpty() ||
                binding.lastName.text.isEmpty() ||
                binding.age.text.isEmpty())
    }

    private fun isEmailValid(email: EditText): Boolean =
        android.util.Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()

    private fun callToast(type: String) =
        Toast.makeText(this, type, Toast.LENGTH_SHORT).show()

    private fun makeValidation(): Boolean {
        if (isAllFieldsValid()) {
            return false
        }
        if (!isEmailValid(binding.emailAddress)) {
            return false
        }
        return true
    }

    private fun makeAllFieldsEmpty() {
        for (i in 0 until binding.root.childCount) {
            if (binding.root.getChildAt(i) is EditText) {
                (binding.root.getChildAt(i) as EditText).text.clear()
            }
        }
    }

    private val addUserClick = View.OnClickListener {
        if (!makeValidation()) {
            getString(R.string.error).also { binding.info.text = it }
            binding.info.setTextColor(resources.getColor(R.color.red))
        } else {
            if (!this.userList.containsKey(binding.emailAddress.text.toString())) {
                this.userList.plus(
                    binding.emailAddress.text.toString()
                            to arrayOf(
                        binding.firstName.text.toString(),
                        binding.lastName.text.toString(),
                        binding.age.text.toString().toInt(),
                    )
                )
                binding.info.text = getString(R.string.success)
                binding.info.setTextColor(resources.getColor(R.color.green))
                binding.activeUserLen.text = this.userList.size.toString()
                callToast(getString(R.string.user_added))
            } else {
                callToast(getString(R.string.user_exists))
            }
        }
    }

    private val removeUserClick = View.OnClickListener {
        if (!makeValidation()) {
            getString(R.string.error).also { binding.info.text = it }
            binding.info.setTextColor(resources.getColor(R.color.red))
        } else {
            if (this.userList.containsKey(binding.emailAddress.text.toString())) {
                this.userList -= binding.emailAddress.text.toString()
                this.deleteUserLen++
                binding.activeUserLen.text = this.userList.size.toString()
                binding.deleteUserLen.text = this.deleteUserLen.toString()
                getString(R.string.success).also { binding.info.text = it }
                binding.info.setTextColor(resources.getColor(R.color.green))
            }
        }

    }
    private val updateUserClick = View.OnClickListener {
        if (!makeValidation()) {
            getString(R.string.error).also { binding.info.text = it }
            binding.info.setTextColor(resources.getColor(R.color.red))
        } else {
            if (this.userList.containsKey(binding.emailAddress.text.toString())) {
//                this.userList.containsKey([binding.emailAddress.text.toString()]) = arrayOf(
//                    binding.firstName.text.toString(),
//                    binding.lastName.text.toString(),
//                    binding.age.text.toString().toInt(),
//                )
                binding.emailAddress.text.toString()
                this.deleteUserLen++
                binding.activeUserLen.text = this.userList.size.toString()
                binding.deleteUserLen.text = this.deleteUserLen.toString()
                getString(R.string.success).also { binding.info.text = it }
                binding.info.setTextColor(resources.getColor(R.color.green))
            }
        }
    }


}